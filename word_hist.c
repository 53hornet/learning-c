#include<stdio.h>

/*
 * Prints a histogram of the lengths of words in its input.
 */
main() {
    int c;
    int count;

    while ((c = getchar()) != EOF) {
        if (c == '\n' || c == '\t' || c == ' ') {

            while (count > 0) {
                printf("%s", "*");
                --count;
            }

            if (count > 0) {
                printf("\n");
            }
        } else
            ++count;
    }
}
