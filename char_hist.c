#include<stdio.h>

#define CHARSET_LENGTH 128;

/*
 * Prints a histogram of the lengths of words in its input.
 */
main() {
    int c, i;
    int count;
    int length = CHARSET_LENGTH;
    int c_counts[length];

    for (i = 0; i < length; ++i) {
        c_counts[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        ++c_counts[c];
    }

    for (i = 0; i < length; ++i) {
        count = c_counts[i];

        if (count > 0 && (i != ' ' && i != '\t' && i != '\n' )) {
            printf("%c\t", i);
            printf("%d\n", count);
        }
    }
}
