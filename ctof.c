#include<stdio.h>

#define LOWER 0;
#define UPPER 300;
#define STEP 20;

float ctof(int c);

/*
 * Print Celcius-Fahrenheit table for celcius = 0, 20, ... 300.
 */
main() {
    float fahr, celcius;
    int lower, upper, step;

    lower = LOWER;      // lower limit of temperature table
    upper = UPPER;    // upper limit of temperature table
    step = STEP;      // step size

    celcius = lower;

    printf("%3s %6s\n", "C", "F");

    while (celcius <= upper) {
        fahr = ctof(celcius);
        printf("%3.0f %6.1f\n", celcius, fahr);
        celcius = celcius + step;
    }
}

/*
 * Convert Celcius to Fahrenheit
 */
float ctof(int celcius) {
    return (9.0 / 5.0) * celcius + 32.0;
}
