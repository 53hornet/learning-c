#include<stdio.h>

/*
 * Convert concurrent blanks to single blank.
 */
main() {
    int c;

    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            while ((c = getchar()) == ' ') 
                ;
            putchar(' ');
        }

        putchar(c);
    }
}
