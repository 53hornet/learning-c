#include<stdio.h>

/*
 * Translate invisible whitespace characters to visible
 * representations.
 */
main() {
    int c;

    while ((c = getchar()) != EOF) {
        if (c == 8) {
            printf("\\b");
            continue;
        }
        if (c == 9) {
            printf("\\t");
            continue;
        }
        if (c == '\\') {
            printf("\\\\");
            continue;
        }

        putchar(c);
    }
}
