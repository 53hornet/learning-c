#include<stdio.h>

#define     LOWER   0       // lower limit of table 
#define     UPPER   300     // upper limit of table 
#define     STEP    20      // step size

float ftoc(int fahr);

/*
 * Print Fahrenheit-Celcius table for fahr = 0, 20, ... 300.
 */
main() {
    float fahr, celcius;
    int lower, upper, step;

    lower = LOWER;      // lower limit of temperature table
    upper = UPPER;    // upper limit of temperature table
    step = STEP;      // step size

    fahr = lower;

    printf("%3s %6s\n", "F", "C");

    //    while (fahr <= upper) {
    //        celcius = 5.0 / 9.0 * (fahr - 32.0);
    //        printf("%3.0f %6.1f\n", fahr, celcius);
    //        fahr = fahr + step;
    //    }

    for (fahr = UPPER; fahr >= LOWER; fahr = fahr - STEP) {
        printf("%3.0f %6.1f\n", fahr, ftoc(fahr));
    }
}

float ftoc(int fahr) {
    return (5.0 / 9.0) * (fahr - 32);
}
