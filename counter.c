#include<stdio.h>

/* 
 * Count characters in input; 1st version 
 */
//main() {
//    long nc;
//
//    nc = 0;
//
//    while (getchar() != EOF)
//        ++nc;
//
//
//    printf("%ld\n", nc);
//}

/*
 * Count characters in input; 2nd version
 */
main() {
    long nc;


    for (nc = 0; getchar() != EOF; ++nc)
        ;

    printf("%ld\n", nc);
}
