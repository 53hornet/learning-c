#include<stdio.h>

/*
 * Count whitespace in input
 */
main() {
    int c, n_whitespace;

    n_whitespace = 0;

    while ((c = getchar()) != EOF) {
        if (c == '\n')
            ++n_whitespace;
        if (c == '\t')
            ++n_whitespace;
        if (c == ' ')
            ++n_whitespace;
    }


    printf("%d\n", n_whitespace);
}
