#include<stdio.h>
#define MAXLENGTH 1000

int get_line(char line[], int maxlength);
void copy(char to[], char from[]);

/*
 * MAIN
 * Prints longest line in STDIN.
 */
main() {
    int len; // current line length
    int max; // maximum length seen so far
    char line[MAXLENGTH]; // current input line
    char longest[MAXLENGTH]; // longest line saved here

    max = 0;
    while ((len = get_line(line, MAXLENGTH)) > 0)
        if (len > max) {
            max = len;
            copy(longest, line);
        }
    if (max > 0) // there was a line
        printf("%d\t", max);
        printf("%s", longest);
    return 0;
}

/*
 * GET_LINE
 * Read STDIN into LINE up to MAXLENGTH and return its length. 
 * Returns the length of a the line.
 */
int get_line(char line[], int maxlength) {
    int c, i;

    for (i = 0; i < maxlength - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}

/*
 * COPY
 * Copy FROM into TO; assume TO is big enough.
 */
void copy(char to[], char from[]) {
    int i;

    i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}

