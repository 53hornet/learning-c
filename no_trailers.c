#include<stdio.h>

#define MAXLENGTH 1000

int get_line(char line[], int maxlength);
void strip_line(char line[], int length);
void copy(char to[], char from[]);

/*
 * MAIN
 * Removes all trailing whitespace and blank lines from STDIN.
 */
int main() {
    int len; // current line length
    char line[MAXLENGTH]; // current input line

    while ((len = get_line(line, MAXLENGTH)) > 0) {
        strip_line(line, len);
        if (line[0] != '\0')
            printf("%s\n", line);
    }
    return 0;
}

/*
 * GET_LINE
 * Read STDIN into LINE up to MAXLENGTH and return its length. 
 * Returns the length of a the line.
 */
int get_line(char line[], int maxlength) {
    int c, i;

    for (i = 0; i < maxlength - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}

/*
 * STRIP_LINE
 * Remove all whitespace from end of LINE of given LENGTH.
 */
void strip_line(char line[], int length) {
    int i, c;

    for (i = --length; i >= 0; --i) {
        c = line[i];

        if (c == ' ' || c == '\t' || c == '\n')
            line[i] = '\0';
        else 
            break;
    }
}


/*
 * COPY
 * Copy FROM into TO; assume TO is big enough.
 */
void copy(char to[], char from[]) {
    int i;

    i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}

