#include<stdio.h>

#define MAXLENGTH 1000

int get_line(char line[], int maxlength);
void copy(char to[], char from[]);

/*
 * MAIN
 * Print all lines larger than 80 characters.
 */
main() {
    int len; // current line length
    char line[MAXLENGTH]; // current input line

    while ((len = get_line(line, MAXLENGTH)) > 0)
        if (len > 80) {
            printf("%s", line);
        }
    return 0;
}

/*
 * GET_LINE
 * Read STDIN into LINE up to MAXLENGTH and return its length. 
 * Returns the length of a the line.
 */
int get_line(char line[], int maxlength) {
    int c, i;

    for (i = 0; i < maxlength - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}
